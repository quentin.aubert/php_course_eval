<?php
class Item 
{
    public $name;
    public $price;
    public $weight;

    public function __construct(string $name, int $price, int $weight) 
    {
        $this->name = $name;
        $this->price = $price;
        $this->weight = $weight;
    }

    public function setName($newName)
    {
        if(is_string($newName))
        {
            $this->name = $newName;
        } else
        {
            trigger_error("This not correspond with the attent", E_USER_ERROR);
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function setWeight($newWeight)
    {
        if(is_string($newWeight))
        {
            $this->weight = $newWeight;
        } else
        {
            trigger_error("This not correspond with the attent", E_USER_ERROR);
        }
    }

    public function getWeight()
    {
        return $this->weight;
    }
    
    public function setPrice($newPrice)
    {
        if(is_numeric($newPrice))
        {
            $this->price = $newPrice;
            $this->price = number_format($this->price/100, 2, '.', ' ');
        } else
        {
            trigger_error("This not correspond with the attent", E_USER_ERROR);
        }
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function toString()
    {
        $nbFormat = number_format($this->price/100, 2, '.', ' ');
        return $this->name . ': ' . $nbFormat . ' €';
    }

}