<?php

class Ticket 
{
    private $ref;
    private $price;
    private $tax = 250;

    public function __construct(string $ref, int $price)
    {
        $this->ref = $ref;
        $this ->price = $price;
    }

    public function setRef($newRef)
    {
        if (is_string($newRef)) {
            $this->ref = $newRef;
        } else {
            trigger_error('Event name must be a string', E_USER_ERROR);
        }
    }

    public function getRef()
    {
        return $this->ref;
    }

    public function setPrice($price)
    {
        if (is_numeric($newPrice)) {
            $this->$price = $newPrice;

        } else {
            trigger_error('Event name must be a string', E_USER_ERROR);
        }
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function toString()
    {
        $string = $this->getRef() . ", " . $this->getPrice();
        return $string;
    }
    
    private function label()
    {
        return (string)$this->ref;
    }

    private function cost()
    {
        $this->price = ($this->getPrice() + ($this->getPrice() * $this->tax/1000))*100;
        return "$this->price" . " cents";
    }

    private function taxRatePerTenThousand()
    {}

    public function payable ()
    {
        return $string = $this->label() . " : " . $this->cost();
    }
}