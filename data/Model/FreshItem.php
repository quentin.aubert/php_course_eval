<?php

require_once 'Item.php';

class FreshItem extends Item 
{
    private $bestBeforeDate;

    public function __construct($name, $price, $weight, $bestBeforeDate){
        parent::__construct($name, $price, $weight);
        $this->bestBeforeDate = $bestBeforeDate;
    }

    public function getBestBeforeDate()
    {
        return $this->bestBeforeDate;
    }

    public function setBestBeforeDate($bestBeforeDate)
    {
        if (!is_string($this->bestBeforeDate)) {
            trigger_error("This not correspond with the attent", E_USER_ERROR);
        } else { 
            $dateformat = $this->bestBeforeDate;
            $dateformat = substr_replace($date, '-', 4, 0);
            $dateformat = substr_replace($date, '-', 7, 0);
            return $dateformat;
        }
    }

    public function toString()
    {  
        $string = "Best Before Date : " . $this->bestBeforeDate . "<br>";
        $string .= parent::toString();
        $string .= " : " . $this->getWeight() . "<br>";
        return $string;
    }

}