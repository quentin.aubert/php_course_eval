<?php

require_once 'Item.php';

class ShoppingCart
{  
    private $items = array();
    private $totalWeight = 0;
    private int $id;

    public function __construct()
    {
        if (!isset($GLOBALS['idShoppingCart'])) {
            $GLOBALS['idShoppingCart'] = 1 ;
            $this->id = $GLOBALS['idShoppingCart'];
        } else {

            $GLOBALS['idShoppingCart'] = $GLOBALS['idShoppingCart'] + 1;
            $this->id =  $GLOBALS['idShoppingCart'];
        }
        
    } 

    public function addItem(Item $item)
    {
        $itemWeight = $item->getWeight();
        if(($item->getWeight() + $this->totalWeight) < 10000) {
            array_push($this->items, $item);
            $this->totalWeight += $itemWeight;
        } else {
            trigger_error("This cart cannont contain more than 10kg of articles", E_USER_ERROR);
            return false;
        }   
    }

    public function removeItem(Item $item)
    {
        $itemWeight = $item->getWeight();
        $index = array_search($item, $this->items);
        if($index !== false) {
              unset($this->items[$index]);
              $this->totalWeight -= $itemWeight;
        }
        return false;
    }

    public function itemCount()
    {
        return count($this->items);
    }

    public function getTotalPrice()
    {
        $totalPrice = 0;
        foreach($this->items as $item) {
            $totalPrice += $item->getPrice();
        }
        $totalPrice = number_format($totalPrice/100, 2, '.', '');
        return $totalPrice;
    }

    public function getId() 
    {
        return $this->id;
    }

    public function toString()
    {
        $string = "<br>This ShoppingCart id: " . $this->getId() . " contains : " . $this->itemCount() . " articles and cost: " . $this->getTotalPrice() . " €" . "<br> Items in Cart: <br>";
        foreach($this->items as $item) {
            $string .= $item->toString() . " : " . $item->getWeight() . "g <br>";
        }
        echo $string;
    }
}