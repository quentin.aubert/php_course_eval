<?php

require_once '../Model/Item.php';
require_once '../Model/ShoppingCart.php';
require_once '../Model/FreshItem.php';
require_once '../Model/Ticket.php';

$item = new Item("corn flakes", 500, 5000);

echo '<pre>';
echo($item->getPrice());
echo '</pre>';

echo '<pre>';
echo($item->getName());
echo '</pre>';

echo '<pre>';
echo $item->toString();
echo '</pre>';

$item2 = new Item("chewing gum", 403, 10);

echo '<pre>';
echo $item2->toString();
echo '</pre>';

$item3= new Item("clock", 5000, 10);

$shoppingCart = new ShoppingCart();
$shoppingCart->addItem($item);
$shoppingCart->addItem($item2);
$shoppingCart->addItem($item3);
$shoppingCart->itemCount();
$shoppingCart->removeItem($item2);

echo '<pre>';
var_dump($shoppingCart);
echo '</pre>';

$shoppingCart->toString();

$item4 = new FreshItem("Fresh Meat", 20, 600, "2022-10-12");
echo $item4->toString();

$ticket = new Ticket("RGBY17032012 - Walles-France", 100);
echo $ticket->toString();
echo '<pre>';
echo $ticket->payable();